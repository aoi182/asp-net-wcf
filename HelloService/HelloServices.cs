﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HelloService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "HelloServices" en el código y en el archivo de configuración a la vez.
    public class HelloServices : IHelloServicesChanged
    {
        public string GetMessage(string name)
        {
            return $"Hello {name}";
        }
    }
}
