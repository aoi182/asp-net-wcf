﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HelloService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IHelloServices" en el código y en el archivo de configuración a la vez.
    [ServiceContract(Name = "IHelloServices")]
    public interface IHelloServicesChanged
    {
        [OperationContract]
        string GetMessage(string name);
    }
}
