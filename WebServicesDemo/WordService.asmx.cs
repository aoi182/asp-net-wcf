﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebServicesDemo.Model;

namespace WebServicesDemo
{
    /// <summary>
    /// Descripción breve de ReverseService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WordService : System.Web.Services.WebService
    {

        [WebMethod]
        public string Reverse(string word)
        {
            return string.Join("", word.Reverse());
        }

        [WebMethod]
        public ConcatOutput Concat(ConcatInput input)
        {
            return new ConcatOutput
            {
                Result = string.Format($"{input.A}{input.Separator}{input.B}")
            };
        }
    }
}
