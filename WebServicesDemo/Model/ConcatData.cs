﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServicesDemo.Model
{
    public class ConcatInput
    {
        public string A { get; set; }
        public string B { get; set; }

        public string Separator { get; set; }
    }

    public class ConcatOutput: Output
    {
        public string Result { get; set; }
    }
}