﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EmployeeServiceWsUnitTest
{
    [TestClass]
    public class UnitTestWs
    {
        [TestMethod]
        public void TestSaveEmployee()
        {
            EmployeeServiceRef.EmployeeServiceClient target = new EmployeeServiceRef.EmployeeServiceClient();

            var e = new EmployeeServiceRef.FullTimeEmployee
            {
                Name = "Adonis Cruz Villarraga",
                Gender = EmployeeServiceRef.EmployeeGender.Male,
                DateOfBirthday = new DateTime(1986, 07, 15),
                AnnualSalary = 2500
            };

            var rq = new EmployeeServiceRef.SaveEmployeeRq {Employee = e};

            var rs = target.SavEmployee(rq);

            Assert.AreNotEqual(0, rs.Employee.ID);
        }

        [TestMethod]
        public void TestGetEmployee()
        {
            EmployeeServiceRef.EmployeeServiceClient target = new EmployeeServiceRef.EmployeeServiceClient();

            var rq = new EmployeeServiceRef.GetEmployeeRq {Id = 6};
            
            var e = target.GetEmployee(rq);

            //Assert.AreEqual(-1, e.Employee.ID);
        }
    }
}
