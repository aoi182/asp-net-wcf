﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWordRemotingServices
{
    public interface IWordRemotingServices
    {
        string Reverse(string word);
    }
}
