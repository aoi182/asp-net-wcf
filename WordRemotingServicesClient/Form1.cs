﻿using System;
using System.Windows.Forms;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace WordRemotingServicesClient
{
    public partial class Form1 : Form
    {
        private IWordRemotingServices.IWordRemotingServices client;
        public Form1()
        {
            InitializeComponent();
            TcpChannel channel = new TcpChannel();
            ChannelServices.RegisterChannel(channel);
            client = (IWordRemotingServices.IWordRemotingServices)Activator.GetObject(typeof(IWordRemotingServices.IWordRemotingServices), "tcp://localhost:8080/Reverse");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string word = textBox1.Text;

            label1.Text = client.Reverse(word);
        }
    }
}
