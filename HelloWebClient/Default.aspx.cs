﻿using System;
using HelloWebClient.HelloService;

namespace HelloWebClient
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void HelloOnClick(object sender, EventArgs e)
        {
            HelloServicesClient client = new HelloServicesClient("BasicHttpBinding_IHelloServices");
            lbl.Text = client.GetMessage(txtName.Text);
        }
    }
}