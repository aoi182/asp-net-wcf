﻿using System;
using EmployeeService;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EmployeeServiceUnitTest
{
    [TestClass]
    public class UnitTestClass
    {
        [TestMethod]
        public void TestSaveEmployee()
        {
            EmployeeService.EmployeeService target = new EmployeeService.EmployeeService();

            var rq = new SaveEmployeeRq
            {
                Employee = new FullTimeEmployee
                {
                    Name = "Adonis Cruz Villarraga",
                    Gender = EmployeeService.EmployeeGender.Male,
                    DateOfBirthday = new DateTime(1986, 07, 15),
                    AnnualSalary = 2500
                }
            };

            var re = target.SavEmployee(rq);

            Assert.AreNotEqual(0, re.Employee.Id);
        }

        [TestMethod]
        public void TestGetEmployee()
        {
            EmployeeService.EmployeeService target = new EmployeeService.EmployeeService();

            var rq = new GetEmployeeRq {Id = 6};

            var rs = target.GetEmployee(rq);

            Assert.AreEqual(6, rs.Employee.Id);
        }
    }
}
