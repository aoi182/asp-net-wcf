﻿CREATE PROCEDURE [dbo].[spGetEmployee]
	@Id int = 0
AS
	SELECT e.* 
	from dbo.Employee as e
	where e.Id = @Id;
RETURN 0