﻿CREATE PROCEDURE [dbo].[spSaveEmployee]
	@Id int = 0,
	@Name nvarchar(50),
	@Gender nvarchar(50),
	@DateOfBirthday datetime,
	@Type int,
	@AnnualSalary int,
	@HourlyPay int,
	@HoursWorked int,
	@Out_Id int output
AS
	IF EXISTS (SELECT 1 FROM dbo.Employee AS E WHERE E.Id = @Id)
		BEGIN
			UPDATE dbo.Employee
			SET Name = @Name, Gender = @Gender, DateOfBirthday = @DateOfBirthday, Type = @Type, AnnualSalary = @AnnualSalary, HourlyPay = @HourlyPay, HoursWorked = @HoursWorked
			WHERE Id = @Id;
			set @Out_Id = @Id;
		END
	ELSE
		BEGIN
			INSERT INTO dbo.Employee (Name, Gender, DateOfBirthday, Type, AnnualSalary, HourlyPay, HoursWorked)
			VALUES(@Name, @Gender, @DateOfBirthday, @Type, @AnnualSalary, @HourlyPay, @HoursWorked);
			set @Out_Id = @@IDENTITY;
		END
RETURN 0