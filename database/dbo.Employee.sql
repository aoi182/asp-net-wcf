﻿CREATE TABLE [dbo].[Employee] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (50) NOT NULL,
    [Gender]         NVARCHAR (50) NOT NULL,
    [DateOfBirthday] DATETIME      NOT NULL,
    [Type]           INT           NOT NULL,
    [AnnualSalary]   INT           NULL,
    [HourlyPay]      INT           NULL,
    [HoursWorked]    INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);