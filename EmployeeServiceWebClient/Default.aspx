﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EmployeeServiceWebClient.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                <td>Id</td>
                <td>
                    <asp:TextBox runat="server" TextMode="Number" ID="txtId" Text="0"/>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtId" ForeColor="Red" ErrorMessage="*" ValidationGroup="emp_01" />
                </td>
            </tr>
            <tr>
                <td>Name</td>
                <td>
                    <asp:TextBox runat="server" ID="txtName" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" ForeColor="Red" ErrorMessage="*" ValidationGroup="emp_01" />
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlGender" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlGender" ForeColor="Red" ErrorMessage="*" ValidationGroup="emp_01" />
                </td>
            </tr>
            <tr>
                <td>Date of Birth</td>
                <td>
                    <asp:TextBox runat="server" TextMode="Date" ID="txtBirth" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBirth" ForeColor="Red" ErrorMessage="*" ValidationGroup="emp_01" />
                </td>
            </tr>
            <tr>
                <td>Employee Type</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlType" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlType" ForeColor="Red" ErrorMessage="*" ValidationGroup="emp_01" />
                </td>
            </tr>
            <tr>
                <td>Annual Salary</td>
                <td>
                    <asp:TextBox runat="server" TextMode="Number" ID="txtAnnualSalary" />
                </td>
            </tr>
            <tr>
                <td>Hourly Pay</td>
                <td>
                    <asp:TextBox runat="server" TextMode="Number" ID="txtHourlyPay" />
                </td>
            </tr>
            <tr>
                <td>Hours Worked</td>
                <td>
                    <asp:TextBox runat="server" TextMode="Number" ID="txtHoursWorked" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="btnGet" Text="Get Employee" OnClick="OnClick" />
                </td>
                <td>
                    <asp:Button runat="server" ID="btnSave" Text="Save Employee" OnClick="OnClick" ValidationGroup="emp_01" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label runat="server" ID="lblResult" />
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
