﻿using EmployeeServiceWebClient.EmployeeService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;

namespace EmployeeServiceWebClient
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            LoadGenderList();
            LoadTypeList();
        }

        private void LoadGenderList()
        {
            List<object> list = new List<object>
            {
                new {Gender = "-- select --", Value= ""},
                new {Gender = "Male", Value = "1"},
                new {Gender = "Female", Value = "2"}
            };

            ddlGender.DataSource = list;
            ddlGender.DataTextField = "Gender";
            ddlGender.DataValueField = "Value";
            ddlGender.DataBind();
        }

        private void LoadTypeList()
        {
            List<object> list = new List<object>
            {
                new {Type = "-- select --", Value = ""},
                new {Type = "Full Time", Value = "1"},
                new {Type = "Partial Time", Value = "2"}
            };

            ddlType.DataSource = list;
            ddlType.DataTextField = "Type";
            ddlType.DataValueField = "Value";
            ddlType.DataBind();
        }

        protected void OnClick(object sender, EventArgs e)
        {
            EmployeeServiceClient client = new EmployeeServiceClient();
            
            if (sender == btnGet)
            {
                GetEmployeeRs getRs = client.GetEmployee(new GetEmployeeRq { Id = Convert.ToInt32(string.IsNullOrWhiteSpace(txtId.Text) ? "0" : txtId.Text) });
                LoadEmployeeVm(getRs.Employee);
            }

            if (sender == btnSave)
            {
                List<string> missingvalues = new List<string>();

                switch (ddlType.SelectedValue)
                {
                    case "1":
                        if (string.IsNullOrWhiteSpace(txtAnnualSalary.Text)) missingvalues.Add("Annual Salary");
                        break;

                    case "2":
                        if (string.IsNullOrWhiteSpace(txtHourlyPay.Text)) missingvalues.Add("Hourly Pay");
                        if (string.IsNullOrWhiteSpace(txtHoursWorked.Text)) missingvalues.Add("Hours Worked");
                        break;
                }

                if (missingvalues.Any())
                {
                    string connector = missingvalues.Count > 1 ? "are" : "is";
                    lblResult.Text = $"{string.Join(",", missingvalues)} {connector} required";
                    lblResult.ForeColor = Color.Red;

                    return;
                }

                var empType = (EmployeeType) Convert.ToInt16(ddlType.SelectedValue);

                Employee emp = empType == EmployeeType.FullTimeEmployee
                    ? (Employee) new FullTimeEmployee
                    {
                        AnnualSalary = Convert.ToInt32(txtAnnualSalary.Text)
                    }
                    : (Employee) new PartTimeEmployee
                    {
                        HourlyPay = Convert.ToInt32(txtHourlyPay.Text),
                        HoursWorked = Convert.ToInt32(txtHoursWorked.Text)
                    };

                emp.ID = Convert.ToInt32(txtId.Text);
                emp.Name = txtName.Text;
                emp.Gender = (EmployeeGender) Convert.ToInt16(ddlGender.SelectedValue);
                emp.Type = (EmployeeType) Convert.ToInt16(ddlType.SelectedValue);

                var saveRq = new SaveEmployeeRq
                {
                    Employee = emp
                };

                var dateParts = txtBirth.Text.Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(q => Convert.ToInt16(q)).ToArray();

                saveRq.Employee.DateOfBirthday = new DateTime(dateParts[0], dateParts[1], dateParts[2]);
                
                lblResult.Text = string.Empty;

                SaveEmployeeRs saveRs = client.SavEmployee(saveRq);
                LoadEmployeeVm(saveRs.Employee);
            }
        }

        private void LoadEmployeeVm(Employee employee)
        {
            if (employee == null)
            {
                txtId.Text = "0";
                txtAnnualSalary.Text = txtBirth.Text = txtHourlyPay.Text = txtHoursWorked.Text = txtName.Text = string.Empty;
                return;
            }

            txtBirth.Text = employee.DateOfBirthday.ToString("yyyy-MM-dd");
            txtId.Text = employee.ID.ToString();
            txtName.Text = employee.Name;
            ddlGender.SelectedValue = ((int)employee.Gender).ToString();
            ddlType.SelectedValue = ((int)employee.Type).ToString();

            if (employee is FullTimeEmployee tmpF)
            {
                txtAnnualSalary.Text = tmpF.AnnualSalary.ToString();
                txtHourlyPay.Text = string.Empty;
                txtHoursWorked.Text = string.Empty;
            }

            if (employee is PartTimeEmployee tmpP)
            {
                txtAnnualSalary.Text = string.Empty;
                txtHourlyPay.Text = tmpP.HourlyPay.ToString();
                txtHoursWorked.Text = tmpP.HoursWorked.ToString();
            }
        }
    }
}