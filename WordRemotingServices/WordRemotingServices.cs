﻿using System;
using System.Linq;

namespace WordRemotingServices
{
    public class WordRemotingServices : MarshalByRefObject, IWordRemotingServices.IWordRemotingServices
    {
        public string Reverse(string word)
        {
            return string.Join("", word.Reverse());
        }
    }
}
