﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "EmployeeService" en el código y en el archivo de configuración a la vez.
    public class EmployeeService : IEmployeeService
    {
        public GetEmployeeRs GetEmployee(GetEmployeeRq rq)
        {
            var rs = new GetEmployeeRs { DateTime = DateTime.Now };

            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

                using (SqlConnection connection = new SqlConnection(cs))
                {
                    SqlCommand command = new SqlCommand("spGetEmployee", connection) { CommandType = CommandType.StoredProcedure };
                    SqlParameter parameterId = new SqlParameter { ParameterName = "@Id", Value = rq.Id };
                    command.Parameters.Add(parameterId);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Employee employee;

                        EmployeeType employeeType = (EmployeeType)reader["Type"];
                        
                        switch (employeeType)
                        {
                            case EmployeeType.FullTimeEmployee:
                                employee = new FullTimeEmployee
                                {
                                    AnnualSalary = (int)reader["AnnualSalary"]
                                };
                                break;

                            case EmployeeType.PartTimeEmployee:
                                employee = new PartTimeEmployee
                                {
                                    HourlyPay = (int)reader["HourlyPay"],
                                    HoursWorked = (int)reader["HoursWorked"]
                                };
                                break;
                            default:
                                return new GetEmployeeRs
                                {
                                    Error = $"El empleado Id {rq.Id}, No tiene un tipo de empleado valido",
                                    HasError = true
                                };
                        }

                        employee.Id = rq.Id;
                        employee.Gender = (EmployeeGender)reader["Gender"];
                        employee.DateOfBirthday = Convert.ToDateTime(reader["DateOfBirthday"]);
                        employee.Name = reader["Name"].ToString();

                        employee.Type = employeeType;

                        rs.Employee = employee;
                    }
                }
            }
            catch (Exception e)
            {
                rs.HasError = true;
                rs.Error = e.Message;
            }

            return rs;
        }

        public SaveEmployeeRs SavEmployee(SaveEmployeeRq rq)
        {
            SaveEmployeeRs rs = new SaveEmployeeRs {DateTime = DateTime.Now};

            try
            {
                string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

                using (SqlConnection connection = new SqlConnection(cs))
                {
                    SqlCommand command = new SqlCommand("spSaveEmployee", connection) { CommandType = CommandType.StoredProcedure };

                    List<SqlParameter> parameters = new List<SqlParameter>
                    {
                        new SqlParameter {ParameterName = "@Id", Value = rq.Employee.Id},
                        new SqlParameter {ParameterName = "@Name", Value = rq.Employee.Name},
                        new SqlParameter {ParameterName = "@Gender", Value = rq.Employee.Gender},
                        new SqlParameter {ParameterName = "@DateOfBirthday", Value = rq.Employee.DateOfBirthday}
                    };

                    if (rq.Employee is FullTimeEmployee tmp1)
                    {
                        parameters.Add(new SqlParameter
                        {
                            ParameterName = "@AnnualSalary",
                            Value = tmp1.AnnualSalary
                        });
                        parameters.Add(new SqlParameter
                        {
                            ParameterName = "@Type",
                            Value = (int)EmployeeType.FullTimeEmployee
                        });
                    }

                    if (rq.Employee is PartTimeEmployee tmp)
                    {
                        parameters.Add(new SqlParameter
                        {
                            ParameterName = "@HourlyPay",
                            Value = tmp.HourlyPay
                        });
                        parameters.Add(new SqlParameter
                        {
                            ParameterName = "@HoursWorked",
                            Value = tmp.HoursWorked
                        });
                        parameters.Add(new SqlParameter
                        {
                            ParameterName = "@Type",
                            Value = (int)EmployeeType.PartTimeEmployee
                        });
                    }

                    var outId = new SqlParameter
                    {
                        ParameterName = "@Out_Id",
                        DbType = DbType.Int32,
                        Direction = ParameterDirection.Output
                    };

                    command.Parameters.AddRange(parameters.ToArray());
                    command.Parameters.Add(outId);

                    connection.Open();
                    command.ExecuteNonQuery();

                    rq.Employee.Id = Convert.ToInt32(outId.Value);
                }
            }
            catch (Exception ex)
            {
                rs.HasError = true;
                rs.Error = ex.Message;
            }

            rs.Employee = rq.Employee;

            return rs;
        }
    }
}
