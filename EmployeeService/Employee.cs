﻿using System;
using System.Runtime.Serialization;

namespace EmployeeService
{
    [KnownType(typeof(FullTimeEmployee))]
    [KnownType(typeof(PartTimeEmployee))]
    [DataContract(Namespace = "http://aoidev.co/Employee")]
    public class Employee
    {
        [DataMember(Name = "ID", Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public EmployeeGender Gender { get; set; }

        [DataMember(Order = 4)]
        public DateTime DateOfBirthday { get; set; }

        [DataMember(Order = 5)]
        public EmployeeType Type { get; set; }

        public DateTime DatoX { get; set; }
    }

    public class FullTimeEmployee : Employee
    {
        public int AnnualSalary { get; set; }
    }

    public class PartTimeEmployee : Employee
    {
        public int HourlyPay { get; set; }
        public int HoursWorked { get; set; }
    }

    public enum EmployeeType
    {
        FullTimeEmployee = 1,
        PartTimeEmployee = 2
    }

    public enum EmployeeGender
    {
        Male = 1,
        Female = 2
    }
}