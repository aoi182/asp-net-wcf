﻿using System;

namespace EmployeeService
{
    public abstract class Rs
    {
        public DateTime DateTime { get; set; }
        public bool HasError { get; set; }
        public string Error { get; set; }
    }

    public class GetEmployeeRq
    {
        public int Id { get; set; }
    }

    public class GetEmployeeRs : Rs
    {
        public Employee Employee { get; set; }
    }

    public class SaveEmployeeRq
    {
        public Employee Employee { get; set; }
    }

    public class SaveEmployeeRs : Rs
    {
        public Employee Employee { get; set; }
    }
}