﻿using System.ServiceModel;

namespace EmployeeService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IEmployeeService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IEmployeeService
    {
        [OperationContract]
        GetEmployeeRs GetEmployee(GetEmployeeRq rq);

        [OperationContract]
        SaveEmployeeRs SavEmployee(SaveEmployeeRq rq);
    }
}