﻿using System;
using System.ServiceModel;

namespace HelloServiceHost
{
    class Program
    {
        static void Main()
        {
            using (ServiceHost host = new ServiceHost(typeof(HelloService.HelloServices)))
            {
                host.Open();
                Console.WriteLine("host started @ " + DateTime.Now);
                Console.ReadLine();
            }
        }
    }
}