﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebServiceClient.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox runat="server" ID="txtWord"></asp:TextBox>
            <asp:Button runat="server" Text="Reverse" OnClick="ReverseOnClick" /><br />
            <asp:Label runat="server" ID="lblReverse" Font-Bold="True" ForeColor="green" />
        </div>
        <hr />
        <div>
            <asp:TextBox runat="server" ID="txtA" />
            <asp:TextBox runat="server" ID="txtB" />
            <asp:Button runat="server" Text="Concat" OnClick="ConcatOnClick" /><br />
            <asp:Label runat="server" ID="lblConcat" Font-Bold="True" ForeColor="green" />
        </div>
    </form>
</body>
</html>
