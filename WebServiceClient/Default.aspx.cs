﻿using System;
using System.Diagnostics;
using WebServiceClient.WordServices;

namespace WebServiceClient
{
    public partial class Default : System.Web.UI.Page
    {
        private readonly WordServices.WordServiceSoapClient _client = new WordServices.WordServiceSoapClient();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ReverseOnClick(object sender, EventArgs e)
        {
            lblReverse.Text = _client.Reverse(txtWord.Text);
        }

        protected void ConcatOnClick(object sender, EventArgs e)
        {
            var input = new ConcatInput
            {
                A = txtA.Text,
                B = txtB.Text,
                Separator = " - "
            };
            
            var r = _client.Concat(input);
            lblConcat.Text = r.Result;

            Debug.WriteLine(r.DateTime);
        }
    }
}