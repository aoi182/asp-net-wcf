﻿using System;

namespace CompanyWebClient
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnClick(object sender, EventArgs e)
        {
            if (sender == btnPublic)
            {
                var client = new CompanyService.CompanyPublicServiceClient("BasicHttpBinding_ICompanyPublicService");
                lblInfo.Text = client.GetPublicInformation();
            }

            if (sender == btnConfidential)
            {
                var client = new CompanyService.CompanyConfidentialServiceClient("NetTcpBinding_ICompanyConfidentialService");
                lblInfo.Text = client.GetConfidentialInformation();
            }
        }
    }
}