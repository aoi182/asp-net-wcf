﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CompanyWebClient.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Button runat="server" Text="Public Information" OnClick="OnClick" ID="btnPublic"/>
        <asp:Button runat="server" Text="Confidential Information" OnClick="OnClick" ID="btnConfidential"/>
        <asp:Label runat="server" ID="lblInfo"/>
    </form>
</body>
</html>
