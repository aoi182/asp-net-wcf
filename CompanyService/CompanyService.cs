﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CompanyService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "CompanyService" en el código y en el archivo de configuración a la vez.
    public class CompanyService : ICompanyPublicService, ICompanyConfidentialService
    {
        public string GetPublicInformation()
        {
            return "This is public information over HTTP to all general public outside the Firewall";
        }

        public string GetConfidentialInformation()
        {
            return "This is confidencial information an only available over TCP behind the company firewall";
        }
    }
}
